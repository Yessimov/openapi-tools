export interface OpenAPIObject {
  openapi: string;
  info: InfoObject;
  servers?: ServerObject[];
  paths: PathsObject;
  components?: ComponentsObject;
  security?: SecurityRequirementObject[];
  tags?: TagObject[];
  externalDocs?: ExternalDocumentationObject;
}

export interface InfoObject {
  title: string;
  description?: string;
  termsOfService?: string;
  contact?: ContactObject;
  license?: LicenseObject;
  version: string;
}

export interface ContactObject {
  name?: string;
  url?: string;
  email?: string;
}

export interface LicenseObject {
  name: string;
  url?: string;
}

export interface ServerObject {
  url: string;
  description?: string;
  variables?: Record<string, ServerVariableObject>;
}

export interface ServerVariableObject {
  enum?: string[];
  default: string;
  description?: string;
}

export interface ComponentsObject {
  schemas?: Record<string, SchemaObject | ReferenceObject>;
  responses?: Record<string, ResponseObject | ReferenceObject>;
  parameters?: Record<string, ParameterObject | ReferenceObject>;
  examples?: Record<string, ExampleObject | ReferenceObject>;
  requestBodies?: Record<string, RequestBodyObject | ReferenceObject>;
  headers?: Record<string, HeaderObject | ReferenceObject>;
  securitySchemes?: Record<string, SecuritySchemeObject | ReferenceObject>;
  links?: Record<string, LinkObject | ReferenceObject>;
  callbacks?: Record<string, CallbackObject | ReferenceObject>;
}

export interface PathsObject {
  [path: string]: PathItemObject;
}

export interface PathItemObject {
  $ref?: string;
  summary?: string;
  description?: string;
  get?: OperationObject;
  put?: OperationObject;
  post?: OperationObject;
  delete?: OperationObject;
  options?: OperationObject;
  head?: OperationObject;
  patch?: OperationObject;
  trace?: OperationObject;
  servers?: ServerObject[];
  parameters?: Array<ParameterObject | ReferenceObject>;
}

export interface OperationObject {
  tags?: string[];
  summary?: string;
  description?: string;
  externalDocs?: ExternalDocumentationObject;
  operationId?: string;
  parameters?: Array<ParameterObject | ReferenceObject>;
  requestBody?: RequestBodyObject | ReferenceObject;
  responses: ResponsesObject;
  callbacks?: Record<string, CallbackObject | ReferenceObject>;
  deprecated?: boolean;
  security?: SecurityRequirementObject[];
  servers?: ServerObject[];
}

export interface ExternalDocumentationObject {
  description?: string;
  url: string;
}

export type ParameterObject = (
  | PathParameterObject
  | QueryOrHeaderOrCookieParameterObject
) &
  (SchemaParameterObject | ContentParameterObject) & {
    name: string;
    description: string;
    deprecated?: boolean;
    allowEmptyValue?: boolean;
    style?: string;
    explode?: boolean;
    allowReserved?: boolean;
    example?: unknown;
    examples?: Record<string, ExampleObject | ReferenceObject>;
  };

export interface PathParameterObject {
  in: "path";
  required: true;
}

export interface QueryOrHeaderOrCookieParameterObject {
  in: "query" | "header" | "cookie";
  required?: boolean;
}

export interface SchemaParameterObject {
  schema?: SchemaObject | ReferenceObject;
}

export interface ContentParameterObject {
  content?: Record<string, MediaTypeObject>;
}

export interface RequestBodyObject {
  description?: string;
  content: Record<string, MediaTypeObject>;
  required?: boolean;
}

export interface MediaTypeObject {
  schema?: SchemaObject | ReferenceObject;
  example?: unknown;
  examples?: Record<string, ExampleObject | ReferenceObject>; // seems to be mutually exclusive with example prop
  encoding?: Record<string, EncodingObject>;
}

export interface EncodingObject {
  contentType?: string; // should we type it?
  headers?: Record<string, HeaderObject | ReferenceObject>;
  style?: string;
  explode?: boolean;
  allowReserved?: boolean;
}

export interface ResponsesObject {
  default?: ResponseObject | ReferenceObject;
  [httpStatusCode: string]: ResponseObject | ReferenceObject; // 1xx, 2xx, 3xx, 4xx, 5xx
}

export interface ResponseObject {
  description: string;
  headers?: Record<string, HeaderObject | ReferenceObject>;
  content?: Record<string, MediaTypeObject>;
  links?: Record<string, LinkObject | ReferenceObject>;
}

export type CallbackObject = Record<Expression, PathItemObject>;

export type ExampleObject = (
  | ExampleObjectWithValue
  | ExampleObjectWithExternalValue
) & {
  summary?: string;
  description?: string;
};

export interface ExampleObjectWithValue {
  value?: unknown;
}

export interface ExampleObjectWithExternalValue {
  externalValue?: string;
}

export type Expression = string; // $url | $method | $statusCode | $request.{unknown} | $response.{unknown}

export type LinkObject = (
  | LinkObjectWithOperationRef
  | LinkObjectWithOperationId
) & {
  parameters?: Record<string, Expression | unknown>;
  requestBody?: Expression | unknown;
  description?: string;
  server?: ServerObject;
};

export interface LinkObjectWithOperationRef {
  operationRef?: string;
}

export interface LinkObjectWithOperationId {
  operationId?: string;
}

export type HeaderObject = Omit<ParameterObject, "name" | "in">;

export interface TagObject {
  name: string;
  description?: string;
  externalDocs?: ExternalDocumentationObject;
}

export interface ReferenceObject {
  $ref: string;
}

export interface SchemaObject {
  title?: string;
  multipleOf?: number;
  maximum?: number;
  exclusiveMaximum?: boolean;
  minimum?: number;
  exclusiveMinimum?: boolean;
  maxLength?: number;
  minLength?: number;
  pattern?: string;
  maxItems?: number;
  minItems?: number;
  uniqueItems?: boolean;
  maxProperties?: number;
  minProperties?: number;
  required?: string[];
  enum?: string[];
  type: "array" | "boolean" | "integer" | "number" | "object" | "string";
  allOf?: Array<SchemaObject | ReferenceObject>;
  oneOf?: Array<SchemaObject | ReferenceObject>;
  anyOf?: Array<SchemaObject | ReferenceObject>;
  not?: Array<SchemaObject | ReferenceObject>;
  items?: SchemaObject | ReferenceObject;
  properties?: Record<string, SchemaObject | ReferenceObject>;
  additionalProperties: SchemaObject | ReferenceObject | boolean;
  description?: string;
  format?:
    | "int32"
    | "int64"
    | "float"
    | "double"
    | "byte"
    | "binary"
    | "string"
    | "boolean"
    | "date"
    | "date-time"
    | "password";
  default?: unknown;
  nullable?: boolean;
  discriminator?: DiscriminatorObject;
  readOnly?: boolean;
  writeOnly?: boolean;
  xml?: XMLObject;
  externalDocs?: ExternalDocumentationObject;
  example?: unknown;
  deprecated?: boolean;
}

export interface DiscriminatorObject {
  propertyName: string;
  mapping?: Record<string, string>;
}

export interface XMLObject {
  name?: string;
  namespace?: string;
  prefix?: string;
  attribute?: boolean;
  wrapper?: boolean;
}

export type SecuritySchemeObject = (
  | ApiKeySecuritySchemeObject
  | HttpSecuritySchemeObject
  | OAuthSecuritySchemeObject
  | OpenIdConnectSecuritySchemeObject
) & {
  description?: string;
};

export interface ApiKeySecuritySchemeObject {
  type: "apiKey";
  name: string;
  in: "query" | "header" | "cookie";
}

export interface HttpSecuritySchemeObject {
  type: "http";
  scheme: string;
  bearerFormat?: string;
}

export interface OAuthSecuritySchemeObject {
  type: "oauth2";
  flows: OAuthFlowsObject;
}

export interface OAuthFlowsObject {
  implicit?: OAuthFlowObject;
  password?: OAuthFlowObject;
  clientCredentials?: OAuthFlowObject;
  authorizationCode: OAuthFlowObject;
}

export interface OAuthFlowObject {
  authorizationUrl: string;
  tokenUrl: string;
  refreshUrl?: string;
  scopes: Record<string, string>;
}

export interface OpenIdConnectSecuritySchemeObject {
  type: "openIdConnect";
  openIdConnectUrl: string;
}

export interface SecurityRequirementObject {
  [name: string]: string[];
}
