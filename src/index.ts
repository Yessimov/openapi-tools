import cac from "cac";

import { generate } from "./parser";

const cli = cac();

cli
  .command("generate", "Generate API client")
  .option("-s, --schema <schhema>", "OpenAPI schema file")
  .option("-d, --destination <destination>", "API client destination folder")
  .action(async (props) => {
    console.log(props);
    await generate(props.schema, props.destination);
  });

cli.version("0.0.1");

cli.parse();
