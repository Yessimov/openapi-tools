import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import {
  OperationObject,
  ParameterObject,
  ReferenceObject,
  RequestBodyObject,
  SchemaObject,
} from "../../types/v3";

interface OperationRequestInterfaceProps {
  raw: OperationObject;
}

function ParameterArraySchema(raw: SchemaObject) {
  return `${ParameterSchema(raw.items)}[]`;
}

function ParameterRefSchema(raw: ReferenceObject) {
  const ref = helpers.schema.refTitle(raw);

  return pascalCase(ref);
}

function ParameterPrimitiveSchema(raw: SchemaObject) {
  switch (raw.format) {
    case "date":
    case "date-time":
      return "Date";
    default:
      return helpers.schema.nodeType(raw);
  }
}

function ParameterSchema(raw: SchemaObject | ReferenceObject) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      console.warn("Path parameter: unsupported object schema");
      return "";
    case "array":
      return ParameterArraySchema(raw as SchemaObject);
    case "ref":
      return ParameterRefSchema(raw as ReferenceObject);
    default:
      return ParameterPrimitiveSchema(raw as SchemaObject);
  }
}

function Parameter(parameter: ParameterObject) {
  if (!("schema" in parameter)) {
    console.warn("Operator parameter: missing schema");
    return "";
  }

  return `${camelCase(parameter.name)}${
    parameter.required ? "" : "?"
  }: ${ParameterSchema(parameter.schema)};`;
}

function OperationRequestInterfaceParameters(
  parameters: Array<ParameterObject | ReferenceObject>
) {
  if (!parameters) return "";

  const queryParameters = parameters
    .filter(helpers.schema.isNotRef)
    .filter((item) => item.in === "query");

  const pathParameters = parameters
    .filter(helpers.schema.isNotRef)
    .filter((item) => item.in === "path");

  return `
      ${queryParameters.length > 0 ? "// query" : ""}
      ${queryParameters.map(Parameter).join("\n")}
      ${pathParameters.length > 0 ? "// path" : ""}
      ${pathParameters.map(Parameter).join("\n")}
    `;
}

function OperationRequestInterfaceBody(
  requestBody: RequestBodyObject | ReferenceObject
) {
  if (!requestBody) return "";

  if ("$ref" in requestBody) {
    console.warn("Operator requestBody: $ref is unsupported");
    return "";
  } else if ("content" in requestBody) {
    const mimeTypes = Object.keys(requestBody.content);

    if (mimeTypes.length > 1) {
      console.warn("Operator requestBody: multiple mime types");
      return "";
    }

    const [mimeType] = mimeTypes;

    const schema = requestBody.content[mimeType].schema;

    const type = helpers.schema.nodeType(schema);

    switch (type) {
      case "ref": {
        const ref = helpers.schema.refTitle(schema as ReferenceObject);

        return `
            body: ${pascalCase(ref)};
          `;
      }
      default: {
        console.warn("Operator requestBody: unsupported schema type");
        return "";
      }
    }
  } else {
    console.warn("Operator requestBody: incorrect requestBody");
    return "";
  }
}

export function OperationRequestInterface({
  raw,
}: OperationRequestInterfaceProps) {
  try {
    return `
      export interface ${pascalCase(raw.operationId)}Request {
        ${OperationRequestInterfaceParameters(raw.parameters)}
        ${OperationRequestInterfaceBody(raw.requestBody)}
      }
    `;
  } catch (error) {
    console.warn(error);
    console.log(JSON.stringify(raw, null, 2));
    return "";
  }
}
