import { OperationObject } from "../../types/v3";

import { OperationRequestInterface } from "./OperationRequestInterface";
import { OperationRequestToPath } from "./OperationRequestToPath";
import { OperationRequestToQuery } from "./OperationRequestToQuery";
import { OperationRequestToBody } from "./OperationRequestToBody";

interface OperationRequestProps {
  raw: OperationObject;
}

export function OperationRequest({ raw }: OperationRequestProps) {
  try {
    return `
      ${OperationRequestInterface({ raw })}
      ${OperationRequestToPath({ raw })}
      ${OperationRequestToQuery({ raw })}
      ${OperationRequestToBody({ raw })}
    `;
  } catch (error) {
    console.warn(error);
    console.log(JSON.stringify(raw, null, 2));
    return "";
  }
}
