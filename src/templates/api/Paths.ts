import { PathItemObject } from "../../types/v3";

import { Path } from "./Path";

interface PathsProps {
  raw: Record<string, PathItemObject>;
}

export function Paths({ raw }: PathsProps) {
  return Object.keys(raw)
    .map((path) => Path({ raw: raw[path] }))
    .join("\n");
}
