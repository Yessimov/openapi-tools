import { pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { OperationObject, ReferenceObject } from "../../types/v3";

interface OperationRequestToBodyProps {
  raw: OperationObject;
}

export function OperationRequestToBody({ raw }: OperationRequestToBodyProps) {
  if (!raw.requestBody) return "";

  if ("$ref" in raw.requestBody) {
    console.warn("Operator requestBody: $ref is unsupported");
    return "";
  } else if ("content" in raw.requestBody) {
    const mimeTypes = Object.keys(raw.requestBody.content);

    if (mimeTypes.length > 1) {
      console.warn("Operator requestBody: multiple mime types");
      return "";
    }

    const [mimeType] = mimeTypes;

    const schema = raw.requestBody.content[mimeType].schema;

    const type = helpers.schema.nodeType(schema);

    const stringifyDict = {
      "application/json": "JSON.stringify",
      "multipart/form-data": "formify",
      "application/x-www-form-urlencoded": "searchify",
    };

    switch (type) {
      case "ref": {
        const ref = helpers.schema.refTitle(schema as ReferenceObject);

        return `
            export const ${pascalCase(
              raw.operationId
            )}RequestToBody = (value: ${pascalCase(
          raw.operationId
        )}Request) => {
               return ${stringifyDict[mimeType]}(${pascalCase(
          ref
        )}ToJSON(value.body)); 
            }
          `;
      }
      default: {
        console.warn("Operator requestBody: unsupported schema type");
        return "";
      }
    }
  } else {
    console.warn("Operator requestBody: incorrect requestBody");
    return "";
  }
}
