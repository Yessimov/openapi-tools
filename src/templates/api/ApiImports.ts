import { pascalCase } from "change-case";

interface ApiImportsProps {
  refs: string[];
  refsTypeDict: Record<string, string>;
}

export function ApiImports({ refs, refsTypeDict }: ApiImportsProps) {
  const imports = refs.map((ref) => {
    const type = refsTypeDict[ref];

    switch (type) {
      case "enum":
        return pascalCase(ref);
      default:
        return `${pascalCase(ref)}, ${pascalCase(ref)}ToJSON`;
    }
  });

  if (imports.length === 0) return "";

  return `
    import { ${imports.join(", ")} } from "../models-extend";
  `;
}
