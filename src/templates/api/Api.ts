import { PathItemObject } from "../../types/v3";

import { Paths } from "./Paths";
import { ApiImports } from "./ApiImports";

interface ApiProps {
  raw: Record<string, PathItemObject>;
  refs: string[];
  refsTypeDict: Record<string, string>;
}

export function Api({ raw, refs, refsTypeDict }: ApiProps) {
  try {
    return `
    /* eslint-disable @typescript-eslint/naming-convention */
    /* eslint-disable @typescript-eslint/no-empty-interface */
    /* eslint-disable @typescript-eslint/no-unused-vars */\n
    import { formify, searchify } from "../runtime";
    ${ApiImports({ refs, refsTypeDict })}
    ${Paths({ raw })}
    /* eslint-enable @typescript-eslint/no-unused-vars */
    /* eslint-enable @typescript-eslint/no-empty-interface */
    /* eslint-enable @typescript-eslint/naming-convention */
  `;
  } catch (error) {
    console.warn(error);
    console.log(JSON.stringify(raw));
    return "";
  }
}
