import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { OperationObject, ParameterObject } from "../../types/v3";

interface OperationRequestToQueryProps {
  raw: OperationObject;
}

function Parameter(parameter: ParameterObject) {
  if ("schema" in parameter && "format" in parameter.schema) {
    switch (parameter.schema.format) {
      case "date":
      case "date-time":
        return `value.${camelCase(parameter.name)}${
          parameter.required ? "" : "?"
        }.toISOString()`;
      default:
        return `value.${camelCase(parameter.name)}`;
    }
  } else {
    return `value.${camelCase(parameter.name)}`;
  }
}

export function OperationRequestToQuery({ raw }: OperationRequestToQueryProps) {
  if (!raw.parameters) return "";

  const queryParameters: ParameterObject[] = raw.parameters
    .filter(helpers.schema.isNotRef)
    .filter((item) => item.in === "query");

  if (queryParameters.length === 0) return "";

  return `
      export const ${pascalCase(
        raw.operationId
      )}RequestToQuery = (value: ${pascalCase(raw.operationId)}Request) => ({
        ${queryParameters
          .map((parameter) => `${parameter.name}: ${Parameter(parameter)}`)
          .join(",\n")}
      })
    `;
}
