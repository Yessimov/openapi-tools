import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { OperationObject, ParameterObject } from "../../types/v3";

interface OperationRequestToPathProps {
  raw: OperationObject;
}

function Parameter(parameter: ParameterObject) {
  if ("schema" in parameter && "format" in parameter.schema) {
    switch (parameter.schema.format) {
      case "date":
      case "date-time":
        return `value.${camelCase(parameter.name)}${
          parameter.required ? "" : "?"
        }.toISOString()`;
      default:
        return `value.${camelCase(parameter.name)}`;
    }
  } else {
    return `value.${camelCase(parameter.name)}`;
  }
}

export function OperationRequestToPath({ raw }: OperationRequestToPathProps) {
  if (!raw.parameters) return "";

  const pathParameters = raw.parameters
    .filter(helpers.schema.isNotRef)
    .filter((item) => item.in === "path");

  if (pathParameters.length === 0) return "";

  return `
      export const ${pascalCase(
        raw.operationId
      )}RequestToPath = (value: ${pascalCase(raw.operationId)}Request) => ({
        ${pathParameters
          .map((parameter) => `${parameter.name}: ${Parameter(parameter)}`)
          .join(",\n")}
      })
    `;
}
