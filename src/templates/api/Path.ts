import { PathItemObject } from "../../types/v3";
import { OperationRequest } from "./OperationRequest";

interface PathProps {
  raw: PathItemObject;
}

export function Path({ raw }: PathProps) {
  try {
    return `
      ${Object.keys(raw)
        .map((method) => OperationRequest({ raw: raw[method] }))
        .join("\n")}
    `;
  } catch (error) {
    console.warn(error);
    console.log(JSON.stringify(raw, null, 2));
    return "";
  }
}
