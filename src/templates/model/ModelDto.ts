import { pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { ReferenceObject, SchemaObject } from "../../types/v3";

interface ModelDtoProps {
  title: string;
  raw: SchemaObject | ReferenceObject;
  refsTypeDict: Record<string, string>;
}

function EnumSchema({ raw }: { raw: SchemaObject }) {
  return raw.enum
    .map((key) => (typeof key === "number" ? key : `'${key}'`))
    .join(" | ");
}

function PrimitiveSchema({ raw }: { raw: SchemaObject }) {
  switch (raw.format) {
    case "binary":
      return "Blob";
    default:
      return helpers.schema.nodeType(raw);
  }
}

function RefSchema({
  raw,
  refsTypeDict,
}: {
  raw: ReferenceObject;
  refsTypeDict: Record<string, string>;
}) {
  const ref = helpers.schema.refTitle(raw);

  const type = refsTypeDict[ref];

  switch (type) {
    case "enum":
      return `${pascalCase(ref)}`;
    default:
      return `${pascalCase(ref)}Dto`;
  }
}

function ArraySchema({
  raw,
  refsTypeDict,
}: {
  raw: SchemaObject;
  refsTypeDict: Record<string, string>;
}) {
  return `${Schema({ raw: raw.items, refsTypeDict })}[]`;
}

function ObjectSchema({
  raw,
  refsTypeDict,
}: {
  raw: SchemaObject;
  refsTypeDict: Record<string, string>;
}) {
  if (!raw.properties) {
    return "unknown";
  }

  return Object.keys(raw.properties)
    .map((key) => {
      return `${key}${helpers.schema.isRequired(raw, key) ? "" : "?"}: ${Schema(
        {
          raw: raw.properties[key],
          refsTypeDict,
        }
      )};`;
    })
    .join("\n");
}

function Schema({
  raw,
  refsTypeDict,
}: {
  raw: SchemaObject | ReferenceObject;
  refsTypeDict: Record<string, string>;
}) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return ObjectSchema({ raw: raw as SchemaObject, refsTypeDict });
    case "ref":
      return RefSchema({ raw: raw as ReferenceObject, refsTypeDict });
    case "array":
      return ArraySchema({ raw: raw as SchemaObject, refsTypeDict });
    case "enum":
      return EnumSchema({ raw: raw as SchemaObject });
    default:
      return PrimitiveSchema({ raw: raw as SchemaObject });
  }
}

export function ModelDto({ raw, title, refsTypeDict }: ModelDtoProps) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return `export interface ${pascalCase(title)}Dto {
          ${Schema({ raw, refsTypeDict })}
      }`;
    case "enum":
      // Prevent duplicate enum declarations
      return "";
    default:
      console.warn(`Model dto: incorrect root type - ${type}`);
      return "";
  }
}
