import { pascalCase } from "change-case";

interface ModelImportsProps {
  refs: string[];
  refsTypeDict: Record<string, string>;
}

export function ModelImports({ refs, refsTypeDict }: ModelImportsProps) {
  return refs
    .map((ref) => {
      const type = refsTypeDict[ref];

      switch (type) {
        case "enum":
          return `import { ${pascalCase(ref)} } from "./${pascalCase(ref)}";`;
        default:
          return `import { ${pascalCase(ref)}, ${pascalCase(
            ref
          )}ToJSON, ${pascalCase(ref)}FromJSON, ${pascalCase(
            ref
          )}Dto } from "./${pascalCase(ref)}";`;
      }
    })
    .join("\n");
}
