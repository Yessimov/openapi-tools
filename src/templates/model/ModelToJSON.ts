import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { ReferenceObject, SchemaObject } from "../../types/v3";

interface ModelToJSONProps {
  title: string;
  raw: SchemaObject | ReferenceObject;
  refsTypeDict: Record<string, string>;
}

function isTransformable(
  raw: SchemaObject | ReferenceObject,
  refsTypeDict: Record<string, string>
) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "ref": {
      const ref = helpers.schema.refTitle(raw as ReferenceObject);

      const innerType = refsTypeDict[ref];

      return innerType !== "enum";
    }
    case "array": {
      return isTransformable((raw as SchemaObject).items, refsTypeDict);
    }
    default:
      return (
        (raw as SchemaObject).format === "date" ||
        (raw as SchemaObject).format === "date-time"
      );
  }
}

function ObjectSchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  if (!raw.properties) {
    return `value.${key}`;
  }

  return Object.keys(raw.properties)
    .map((key) => {
      return `${key}: ${
        !helpers.schema.isRequired(raw, key) &&
        isTransformable(raw.properties[key], refsTypeDict)
          ? `value.${camelCase(key)} === undefined ? undefined : ${Schema({
              raw: raw.properties[key],
              key,
              refsTypeDict,
            })}`
          : Schema({
              raw: raw.properties[key],
              key,
              refsTypeDict,
            })
      },`;
    })
    .join("\n");
}

function RefSchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: ReferenceObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const ref = helpers.schema.refTitle(raw);

  const type = refsTypeDict[ref];

  switch (type) {
    case "enum":
      return `value.${key}`;
    default:
      return `${pascalCase(ref)}ToJSON(value.${key})`;
  }
}

function ArraySchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const type = helpers.schema.nodeType(raw.items);

  switch (type) {
    case "ref":
      const ref = helpers.schema.refTitle(raw.items as ReferenceObject);

      const type = refsTypeDict[ref];

      switch (type) {
        case "enum":
          return `value.${key}`;
        default:
          return `value.${key}.map(${pascalCase(ref)}ToJSON)`;
      }

    default:
      return `value.${key}`;
  }
}

function PrimitiveSchema({ raw, key }: { raw: SchemaObject; key: string }) {
  switch (raw.format) {
    case "date":
      return `value.${key}.toISOString().substr(0, 10)`;
    case "date-time":
      return `value.${key}.toISOString()`;
    default:
      return `value.${key}`;
  }
}

function Schema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject | ReferenceObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return ObjectSchema({
        raw: raw as SchemaObject,
        key: camelCase(key),
        refsTypeDict,
      });
    case "ref":
      return RefSchema({
        raw: raw as ReferenceObject,
        key: camelCase(key),
        refsTypeDict,
      });
    case "array":
      return ArraySchema({
        raw: raw as SchemaObject,
        key: camelCase(key),
        refsTypeDict,
      });
    default:
      return PrimitiveSchema({
        raw: raw as SchemaObject,
        key: camelCase(key),
      });
  }
}

export function ModelToJSON({ raw, title, refsTypeDict }: ModelToJSONProps) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return `
      export function ${pascalCase(title)}ToJSON(value: ${pascalCase(
        title
      )}): ${pascalCase(title)}Dto;
      export function ${pascalCase(title)}ToJSON(value: ${pascalCase(
        title
      )} | null | undefined): ${pascalCase(title)}Dto | null | undefined {
          if (value === null || value === undefined) return value;\n
          return {
            ${Schema({ raw, key: "", refsTypeDict })}
          }
      }`;
    default:
      return "";
  }
}
