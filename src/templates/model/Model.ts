import { ReferenceObject, SchemaObject } from "../../types/v3";
import { ModelDto } from "./ModelDto";
import { ModelToJSON } from "./ModelToJSON";
import { ModelImports } from "./ModelImports";
import { ModelInterface } from "./ModelInterface";
import { ModelFromJSON } from "./ModelFromJSON";

interface ModelProps {
  title: string;
  raw: SchemaObject | ReferenceObject;
  refs: string[];
  refsTypeDict: Record<string, string>;
}

export function Model({ title, raw, refs, refsTypeDict }: ModelProps) {
  try {
    return `
    /* eslint-disable @typescript-eslint/naming-convention */
    /* eslint-disable @typescript-eslint/no-unused-vars */\n
    import { exists } from "../runtime";
    ${ModelImports({ refs, refsTypeDict })}
    ${ModelInterface({ title, raw })}\n
    ${ModelDto({ title, raw, refsTypeDict })}\n
    ${ModelFromJSON({ title, raw, refsTypeDict })}\n
    ${ModelToJSON({ title, raw, refsTypeDict })}\n
    /* eslint-enable @typescript-eslint/no-unused-vars */
    /* eslint-enable @typescript-eslint/naming-convention */
    
  `;
  } catch (error) {
    console.warn(error);
    console.log(JSON.stringify(raw, null, 2));
    return "";
  }
}
