import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { ReferenceObject, SchemaObject } from "../../types/v3";

interface ModelFromJSONProps {
  title: string;
  raw: SchemaObject | ReferenceObject;
  refsTypeDict: Record<string, string>;
}

function ObjectSchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  if (!raw.properties) {
    return `json['${key}']`;
  }

  return Object.keys(raw.properties)
    .map((key) => {
      return `${camelCase(key)}: ${
        helpers.schema.isRequired(raw, key)
          ? Schema({ raw: raw.properties[key], key, refsTypeDict })
          : `!exists(json, '${key}') ? undefined : ${Schema({
              raw: raw.properties[key],
              key,
              refsTypeDict,
            })}`
      },`;
    })
    .join("\n");
}

function RefSchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: ReferenceObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const ref = helpers.schema.refTitle(raw);

  const type = refsTypeDict[ref];

  switch (type) {
    case "enum":
      return `json['${key}']`;
    default:
      return `${pascalCase(ref)}FromJSON(json['${key}'])`;
  }
}

function ArraySchema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const type = helpers.schema.nodeType(raw.items);

  switch (type) {
    case "ref":
      const ref = helpers.schema.refTitle(raw.items as ReferenceObject);

      const type = refsTypeDict[ref];

      switch (type) {
        case "enum":
          return `json['${key}']`;
        default:
          return `json['${key}'].map(${pascalCase(ref)}FromJSON)`;
      }
    default:
      return `json['${key}']`;
  }
}

function PrimitiveSchema({ raw, key }: { raw: SchemaObject; key: string }) {
  switch (raw.format) {
    case "date":
    case "date-time":
      return `new Date(json['${key}'])`;
    default:
      return `json['${key}']`;
  }
}

function Schema({
  raw,
  key,
  refsTypeDict,
}: {
  raw: SchemaObject | ReferenceObject;
  key: string;
  refsTypeDict: Record<string, string>;
}) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return ObjectSchema({ raw: raw as SchemaObject, key, refsTypeDict });
    case "ref":
      return RefSchema({ raw: raw as ReferenceObject, key, refsTypeDict });
    case "array":
      return ArraySchema({ raw: raw as SchemaObject, key, refsTypeDict });
    default:
      return PrimitiveSchema({ raw: raw as SchemaObject, key });
  }
}

export function ModelFromJSON({
  title,
  raw,
  refsTypeDict,
}: ModelFromJSONProps) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return `
      export function ${pascalCase(title)}FromJSON(json: any): ${pascalCase(
        title
      )} {
          if (json === null || json === undefined) return json;\n
          return {
            ${Schema({ raw, key: "", refsTypeDict })}
          }
      }`;
    default:
      return "";
  }
}
