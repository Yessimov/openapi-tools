import { camelCase, pascalCase } from "change-case";
import { helpers } from "../../helpers";
import { ReferenceObject, SchemaObject } from "../../types/v3";

interface ModelInterfaceProps {
  title: string;
  raw: SchemaObject | ReferenceObject;
}

function EnumSchema({
  raw,
  root = false,
}: {
  raw: SchemaObject;
  root?: boolean;
}) {
  if (!root) {
    return raw.enum
      .map((key) => (typeof key === "number" ? key : `'${key}'`))
      .join(" | ");
  }

  const isOrder = raw.enum.some(
    (key) => typeof key === "string" && key.startsWith("-")
  );
  const isDigitChar = raw.enum.some(
    (key) => typeof key === "string" && /^\d/.test(key)
  );
  const isDigits = raw.enum.every((key) => typeof key === "number");

  return raw.enum
    .map((key) => {
      if (isOrder) {
        return `${pascalCase(key)}${
          key.startsWith("-") ? "Desc" : "Asc"
        } = '${key}'`;
      } else if (isDigitChar) {
        return `_${pascalCase(key)} = '${key}'`;
      } else if (isDigits) {
        return `_${key} = ${key}`;
      } else {
        return `${pascalCase(key)} = '${key}'`;
      }
    })
    .join(",\n");
}

function PrimitiveSchema({ raw }: { raw: SchemaObject }) {
  switch (raw.format) {
    case "date":
    case "date-time":
      return "Date";
    case "binary":
      return "Blob";
    default:
      return helpers.schema.nodeType(raw);
  }
}

function RefSchema({ raw }: { raw: ReferenceObject }) {
  const ref = helpers.schema.refTitle(raw);

  return pascalCase(ref);
}

function ArraySchema({ raw }: { raw: SchemaObject }) {
  return `${Schema({ raw: raw.items })}[]`;
}

function ObjectSchema({ raw }: { raw: SchemaObject }) {
  if (!raw.properties) {
    return "unknown";
  }

  return Object.keys(raw.properties)
    .map((key) => {
      return `${camelCase(key)}${
        helpers.schema.isRequired(raw, key) ? "" : "?"
      }: ${Schema({ raw: raw.properties[key] })};`;
    })
    .join("\n");
}

function Schema({ raw }: { raw: SchemaObject | ReferenceObject }) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return ObjectSchema({ raw: raw as SchemaObject });
    case "ref":
      return RefSchema({ raw: raw as ReferenceObject });
    case "array":
      return ArraySchema({ raw: raw as SchemaObject });
    case "enum":
      return EnumSchema({ raw: raw as SchemaObject });
    default:
      return PrimitiveSchema({ raw: raw as SchemaObject });
  }
}

export function ModelInterface({ title, raw }: ModelInterfaceProps) {
  const type = helpers.schema.nodeType(raw);

  switch (type) {
    case "object":
      return `export interface ${pascalCase(title)} {
          ${Schema({ raw })}
      }`;
    case "enum":
      return `export enum ${pascalCase(title)} {
          ${EnumSchema({ raw: raw as SchemaObject, root: true })}
      }`;

    default:
      console.warn(`Model interface: incorrect root type - ${type}`);
      return "";
  }
}
