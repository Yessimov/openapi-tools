import { root } from "./root";
import { api } from "./api";
import { schema } from "./schema";

export const helpers = {
  root,
  api,
  schema,
};
