import { OpenAPIObject } from "../types/v3";
import { schema } from "./schema";

function collectRefsTypeDict(openapi: OpenAPIObject) {
  const refsTypeDict: Record<string, string> = {};

  Object.keys(openapi.components.schemas).forEach((key) => {
    refsTypeDict[key] = schema.nodeType(openapi.components.schemas[key]);
  });

  return refsTypeDict;
}

export const root = {
  collectRefsTypeDict,
};
