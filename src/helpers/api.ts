import { PathItemObject, ReferenceObject, SchemaObject } from "../types/v3";
import { schema } from "./schema";

const OPERATIONS = [
  "get",
  "post",
  "put",
  "delete",
  "options",
  "head",
  "patch",
  "trace",
] as const;

function groupPaths(raw: Record<string, PathItemObject>) {
  const apis: Record<string, Record<string, PathItemObject>> = {};

  Object.keys(raw).forEach((key) => {
    const [prefix, version, api] = key.slice(1).split("/");

    apis[api] = apis[api] || {};
    apis[api][key] = raw[key];
  });

  return apis;
}

function traverse({
  raw,
  traverser,
}: {
  raw: Record<string, PathItemObject>;
  traverser: (raw: SchemaObject | ReferenceObject) => void;
}) {
  Object.values(raw).forEach((pathObject) => {
    operations(pathObject).forEach((op) => {
      const opObject = pathObject[op];

      const requestBody = opObject.requestBody;
      const parameters = opObject.parameters;

      if (parameters) {
        parameters.forEach((parameter) => {
          if ("schema" in parameter) {
            schema.traverse({
              raw: parameter.schema,
              traverser,
            });
          }
        });
      }

      if (requestBody) {
        if ("$ref" in requestBody) {
          schema.traverse({
            raw: requestBody,
            traverser,
          });
        } else if ("content" in requestBody) {
          Object.keys(requestBody.content).forEach((mimeType) => {
            schema.traverse({
              raw: requestBody.content[mimeType].schema,
              traverser,
            });
          });
        }
      }
    });
  });
}

function collectRefs(raw: Record<string, PathItemObject>) {
  let refs: string[] = [];

  traverse({
    raw,
    traverser: (item) => {
      const type = schema.nodeType(item);

      if (type === "ref") {
        const ref = schema.refTitle(item as ReferenceObject);
        refs.push(ref);
      }
    },
  });

  // uniq
  refs = refs.filter((value, index) => refs.indexOf(value) === index);

  return refs;
}

function operations(raw: PathItemObject) {
  return OPERATIONS.filter((key) => raw[key] != null);
}

export const api = {
  groupPaths,
  collectRefs,
  operations,
};
