import { ReferenceObject, SchemaObject } from "../types/v3";

function nodeType(obj: any) {
  if (!obj || typeof obj !== "object") {
    return undefined;
  }

  if (obj["$ref"]) {
    return "ref";
  }

  // enum
  if (Array.isArray(obj.enum)) {
    return "enum";
  }

  // boolean
  if (obj.type === "boolean") {
    return "boolean";
  }

  // string
  if (
    ["binary", "byte", "date", "date-time", "password", "string"].includes(
      obj.type
    )
  ) {
    return "string";
  }

  // number
  if (["double", "float", "integer", "number"].includes(obj.type)) {
    return "number";
  }

  // anyOf
  if (Array.isArray(obj.anyOf)) {
    return "anyOf";
  }

  // oneOf
  if (Array.isArray(obj.oneOf)) {
    return "oneOf";
  }

  // array
  if (obj.type === "array" || obj.items) {
    return "array";
  }

  // return object by default
  return "object";
}

function isRequired(raw: SchemaObject, key: string) {
  return raw.required ? raw.required.includes(key) : false;
}

function refTitle(raw: ReferenceObject) {
  const [hash, root, parent, ref] = raw.$ref.split("/");
  return ref;
}

function traverse({
  raw,
  traverser,
}: {
  raw: SchemaObject | ReferenceObject;
  traverser: (raw: SchemaObject | ReferenceObject) => void;
}) {
  function _innerTraverseObject(raw: SchemaObject) {
    if (!raw.properties) return;

    Object.keys(raw.properties).forEach((key) => {
      traverser(raw.properties[key]);
      _innerTraverse(raw.properties[key]);
    });
  }
  function _innerTraverseArray(raw: SchemaObject) {
    traverser(raw.items);
    _innerTraverse(raw.items);
  }
  function _innerTraverseRef(raw: ReferenceObject) {
    traverser(raw);
  }
  function _innerTraverseEnum(raw: SchemaObject) {
    traverser(raw);
  }
  function _innerTraverse(raw: SchemaObject | ReferenceObject) {
    const type = nodeType(raw);

    switch (type) {
      case "object":
        _innerTraverseObject(raw as SchemaObject);
      case "array":
        _innerTraverseArray(raw as SchemaObject);
      case "ref":
        _innerTraverseRef(raw as ReferenceObject);
      case "enum":
        _innerTraverseEnum(raw as SchemaObject);
      default:
        return;
    }
  }

  _innerTraverse(raw);
}

function collectRefs(raw: SchemaObject | ReferenceObject) {
  let refs: string[] = [];

  traverse({
    raw,
    traverser: (item) => {
      const type = nodeType(item);

      if (type === "ref") {
        const ref = refTitle(item as ReferenceObject);
        refs.push(ref);
      }
    },
  });

  // uniq
  refs = refs.filter((value, index) => refs.indexOf(value) === index);

  return refs;
}

function isNotRef<T>(value: T | ReferenceObject): value is T {
  return !("$ref" in value);
}

export const schema = {
  nodeType,
  isRequired,
  refTitle,
  collectRefs,
  isNotRef,
  traverse,
};
