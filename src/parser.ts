import fs from "fs";
import { pascalCase } from "change-case";
import https from "https";
import http from "http";
import path from "path";
import url from "url";
import prettier from "prettier";

import { OpenAPIObject } from "./types/v3";
import { helpers } from "./helpers";
import { Model } from "./templates/model";
import { Api } from "./templates/api";

export async function generate(target: string, destination: string) {
  await fs.promises.mkdir(path.join(destination, "models"), {
    recursive: true,
  });
  await fs.promises.mkdir(path.join(destination, "paths"), {
    recursive: true,
  });

  const buffer = await load(target);

  const openapi: OpenAPIObject = JSON.parse(buffer.toString());

  const refsTypeDict = helpers.root.collectRefsTypeDict(openapi);

  Object.keys(openapi.components.schemas).forEach(async (key) => {
    const raw = openapi.components.schemas[key];

    const output = Model({
      title: key,
      raw,
      refs: helpers.schema.collectRefs(raw),
      refsTypeDict,
    });

    await fs.promises.writeFile(
      path.join(destination, "models", `${pascalCase(key)}.ts`),
      prettier.format(output, { parser: "typescript" })
    );
  });

  const apis = helpers.api.groupPaths(openapi.paths);

  Object.keys(apis).forEach(async (key) => {
    const raw = apis[key];

    const output = Api({
      raw,
      refs: helpers.api.collectRefs(raw),
      refsTypeDict,
    });

    await fs.promises.writeFile(
      path.join(destination, "paths", `${key}.ts`),
      prettier.format(output, { parser: "typescript" })
    );
  });
}

function load(target: string) {
  if (/^https?:\/\//.test(target)) {
    return loadFromNetwork(target);
  } else {
    return loadFromFile(target);
  }
}

function loadFromFile(target: string) {
  return fs.promises.readFile(target);
}

function loadFromNetwork(target: string) {
  console.log(`🤞 Loading schema from ${target} ...`);

  const { protocol, ...rest } = url.parse(target);

  const fetch: typeof http | typeof https = {
    "http:": http,
    "https:": https,
  }[protocol];

  return new Promise((resolve, reject) => {
    const req = fetch.request(
      {
        method: "GET",
        ...rest,
      },
      (res) => {
        let buffer = "";

        res.setEncoding("utf8");
        res
          .on("data", (chunk: string) => {
            buffer += chunk;
          })
          .on("end", () => {
            resolve(buffer);
          });
      }
    );

    req.on("error", (err) => {
      reject(err);
    });

    req.end();
  });
}
